package com.hovorun.Model;

import java.util.List;

public interface IModel {

    List <Goods> findGoodsCheaperOrEqual(double priceWanted);

    List <Goods> getAllGoodsList();

    List<Goods> getGoodsByCategory(String category);
}
