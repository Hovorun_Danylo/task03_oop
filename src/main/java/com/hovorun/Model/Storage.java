package com.hovorun.Model;

import java.util.*;

import static com.hovorun.Model.GoodsType.*;

public class Storage {

    private Set <Goods> goodsStorage;


    public Storage() {
        goodsStorage = new HashSet <>();
        Goods table = new Goods("Table", 50.0, WOODENGOODS);
        Goods chair = new Goods("Chair", 25.75, WOODENGOODS);
        Goods locker = new Goods("Locker", 15.99, WOODENGOODS);

        Goods sink = new Goods("Sink", 100.00, PLUMBING);
        Goods toilet = new Goods("Toilet", 88.40, PLUMBING);
        Goods pipe = new Goods("Pipe", 9.99, PLUMBING);

        goodsStorage.add(table);
        goodsStorage.add(chair);
        goodsStorage.add(locker);
        goodsStorage.add(sink);
        goodsStorage.add(toilet);
        goodsStorage.add(pipe);
    }

    public Set <Goods> getGoodsStorage() {
        return goodsStorage;
    }
    public List<Goods> findGoodsCheaperOrEqual(double priceWanted){

        List<Goods> sortedByPriceList = new ArrayList <>();
        for (Goods g: goodsStorage) {
            if(g.getGoodsPrice() <= priceWanted){
                sortedByPriceList.add(g);
            }
        }

        return sortedByPriceList;
    }
}
