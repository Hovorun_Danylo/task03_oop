package com.hovorun.Model;

public enum GoodsType {
    WOODENGOODS("Wooden goods"),
    PLUMBING("Plumbing goods");

    private final String name;

    private GoodsType(final String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
