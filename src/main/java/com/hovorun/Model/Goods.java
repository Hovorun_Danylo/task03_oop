package com.hovorun.Model;


public class Goods {

    private String goodsName;
    private double goodsPrice;
    private GoodsType goodsType;

    public Goods(String goodsName, double goodsPrice, GoodsType goodsType) {
        this.goodsName = goodsName;
        this.goodsPrice = goodsPrice;
        this.goodsType = goodsType;
    }

    public GoodsType getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(GoodsType goodsType) {
        this.goodsType = goodsType;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    @Override
    public String toString() {
        return goodsName + ": price = $" + goodsPrice;
    }
}
