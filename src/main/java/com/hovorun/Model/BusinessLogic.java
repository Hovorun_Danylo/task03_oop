package com.hovorun.Model;

import java.util.*;

public class BusinessLogic implements IModel {

    private Storage storage;

    public BusinessLogic() {
        storage = new Storage();
    }

    @Override
    public List <Goods> findGoodsCheaperOrEqual(double priceWanted) {
        return storage.findGoodsCheaperOrEqual(priceWanted);
    }

    @Override
    public List <Goods> getAllGoodsList() {
        return new ArrayList<>(List.copyOf(storage.getGoodsStorage()));
    }

    @Override
    public List <Goods> getGoodsByCategory(String category) {

        List<Goods> goodsByCategoryList = new ArrayList <>();
        for (Goods g: storage.getGoodsStorage()) {
            if (g.getGoodsType().getName().equalsIgnoreCase(category)){
                goodsByCategoryList.add(g);
            }
        }

        return goodsByCategoryList;
    }
}
