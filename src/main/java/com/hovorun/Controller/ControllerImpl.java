package com.hovorun.Controller;

import com.hovorun.Model.BusinessLogic;
import com.hovorun.Model.Goods;
import com.hovorun.Model.IModel;

import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements IController {

    private IModel model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List <Goods> generateGoodsLessOrEqualByPrice(double price) {
        return model.findGoodsCheaperOrEqual(price);
    }

    @Override
    public List <Goods> generateGoodsList() {
        return model.getAllGoodsList();
    }

    @Override
    public List <Goods> getGoodsByCategory(String category) {
        return model.getGoodsByCategory(category);
    }
}
