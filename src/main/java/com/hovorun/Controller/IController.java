package com.hovorun.Controller;

import com.hovorun.Model.Goods;

import java.util.ArrayList;
import java.util.List;

public interface IController {

    List <Goods> generateGoodsLessOrEqualByPrice(double price);

    List <Goods> generateGoodsList();

    List<Goods> getGoodsByCategory(String category);
}
